#!/usr/bin/env python3
"""
This script will take two arguments: video and audio file names
It will replace the audio in the video file and save the new file in the same
directory with video name with a '-clean' suffix before the extension.
"""

import sys
import subprocess
import re

args = sys.argv

if len(args) < 3:
    print("Two arguments are needed which are not satisfied!")
    exit(1)
elif (len(args) > 3):
    print("More than two arguments are provided!")
    exit(1)

video_file = args[1]
audio_file = args[2]

(filename, extension) = re.findall(r'^(.+)\.([^.]+)$', video_file)[0]

video_clean_file = "{}-clean.{}".format(filename, extension)

command = "ffmpeg -i '{}' -i '{}' -c:v copy -map 0:v:0 -map 1:a:0 '{}'".format(
    video_file,
    audio_file,
    video_clean_file)

subprocess.run(command, shell=True)