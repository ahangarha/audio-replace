# Audio Replace

## About

Replace audio in a video file with another given audio file. This script uses
`FFMPEG` to process and is actually a simplifier tool for it.

## How to use

Simply give video and audio files as argument like this:

```
$ audio-replace videofile audiofile
```

The script will generate a new video file where the videofile is located by
adding `-clean` suffix at the end of its name, right before extension.

## License

This script is licensed under GNU General Public License v3.